export const myActivities = [
  {
    id: 1,
    title: "My First Activity",
    description: "This is my first activity",
  },
  {
    id: 2,
    title: "My Second Activity",
    description: "This is my Second activity",
  },
  {
    id: 3,
    title: "My Third Activity",
    description: "This is my Third activity",
  },
  {
    id: 4,
    title: "My fourth Activity",
    description: "This is my fourth activity",
  },
  {
    id: 5,
    title: "My fifth Activity",
    description: "This is my fifth activity",
  },
  {
    id: 6,
    title: "My sixth Activity",
    description: "This is my sixth activity",
  },
];

export const count = myActivities.length;
