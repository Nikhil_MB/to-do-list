import React, { useState } from "react";
import { myActivities } from "../Data/activitiesData";
import UpdateActivity from "./UpdateActivity";

const MyActivities = ({ props = { values: myActivities }, update }) => {
  const [initialData, setInitialData] = useState(props.values);
  const [toggleUpdate, setToggleUpdate] = useState(false);
  const [key, setKey] = useState(0);

  const handleDelete = (id) => {
    setInitialData(initialData.filter((activity) => activity.id !== id));
  };

  const handleUpdate = (id) => {
    setKey(id);
    if (toggleUpdate) {
      setToggleUpdate(false);
    } else {
      setToggleUpdate(true);
    }
  };

  const myActivity = initialData.map((activity) => {
    return (
      <div
        key={activity.id}
        className='card col-3'
        style={{
          textAlign: "left",
          marginLeft: "70px",
          marginTop: "20px",
        }}
      >
        <div className='card-body'>
          <h3 className='card-title'>{activity.title}</h3>
          <p className='card-text'>{activity.description}</p>
          <br />
          <br />
          <button
            type='button'
            className='btn btn-dark'
            onClick={() => handleUpdate(activity.id)}
          >
            Update
          </button>
          <button
            type='button'
            style={{ marginLeft: "80px" }}
            className='btn btn-secondary'
            onClick={() => handleDelete(activity.id)}
          >
            Delete
          </button>
        </div>
      </div>
    );
  });

  return (
    <div className='row'>
      {myActivity}
      {toggleUpdate && (
        <UpdateActivity
          isModelOpen={toggleUpdate}
          updateKey={key}
          updated={update}
        />
      )}
    </div>
  );
};

export default MyActivities;
