import React, { useState } from "react";
import { Modal } from "react-bootstrap";
import { myActivities, count } from "../Data/activitiesData";
import MyActivities from "./MyActivities";

const NewActivity = ({ isModelOpen, updateActivity }) => {
  const defaultState = {
    id: "",
    title: "",
    description: "",
  };

  const [toggleModal, setToggleModal] = useState(isModelOpen);
  const [userData, setUserData] = useState(defaultState);
  const [toggleMyActivities, setToggleMyActivities] = useState(false);

  const handleChange = (event) => {
    const field = event.target.name;
    const value = event.target.value;
    setUserData({ ...userData, [field]: value });
  };

  const handleClose = () => {
    setToggleModal(false);
  };

  const handleCreateActivity = () => {
    let counter = count + 1;
    myActivities.push({
      id: counter,
      title: userData.title,
      description: userData.description,
    });
    setToggleMyActivities(true);
    setToggleModal(false);
  };

  return (
    <div>
      <Modal show={toggleModal}>
        <Modal.Header closeButton>
          <Modal.Title>New Activity</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <form className='card'>
            <label htmlFor='title'>Title</label>
            <input
              type='text'
              name='title'
              id='title'
              value={userData.title}
              onChange={handleChange}
            />
            <label htmlFor='description'>Description</label>
            <textarea
              name='description'
              id='description'
              value={userData.description}
              onChange={handleChange}
            />
            <br />
            <button
              type='button'
              className='btn btn-primary'
              onClick={handleCreateActivity}
            >
              Create
            </button>
            <br />
            <button
              className='btn btn-secondary'
              type='button'
              onClick={handleClose}
            >
              close
            </button>
          </form>
        </Modal.Body>
      </Modal>
      <div>
        {toggleMyActivities && <MyActivities update={updateActivity} />}
      </div>
    </div>
  );
};

export default NewActivity;
