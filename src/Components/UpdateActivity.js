import React, { useState } from "react";
import { Modal } from "react-bootstrap";
import { myActivities } from "../Data/activitiesData";

const UpdateActivity = ({ isModelOpen, updateKey, updated }) => {
  const activityToUpdate = myActivities.filter(
    (activity) => activity.id === updateKey
  );

  const [toggleModal, setToggleModal] = useState(isModelOpen);
  const [updateValues, setupdateValues] = useState(activityToUpdate[0]);

  const handleClose = () => {
    setToggleModal(false);
  };

  const handleChange = (event) => {
    const field = event.target.name;
    const value = event.target.value;
    setupdateValues({ ...updateValues, [field]: value });
  };

  const handleUpdate = (id) => {
    myActivities.forEach((activity) => {
      if (activity.id === id) {
        activity.title = updateValues.title;

        activity.description = updateValues.description;
      }
    });

    setToggleModal(false);
    updated();
  };

  return (
    <div>
      <Modal show={toggleModal}>
        <Modal.Header closeButton>
          <Modal.Title>Update Activity</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <form className='card'>
            <label htmlFor='title'>Title</label>
            <input
              type='text'
              name='title'
              id='title'
              value={updateValues.title}
              onChange={handleChange}
            />
            <label htmlFor='description'>Description</label>
            <textarea
              name='description'
              id='description'
              value={updateValues.description}
              onChange={handleChange}
            />
            <br />
            <button
              type='button'
              className='btn btn-primary'
              onClick={() => handleUpdate(updateValues.id)}
            >
              Update
            </button>
            <br />
            <button
              className='btn btn-secondary'
              type='button'
              onClick={handleClose}
            >
              close
            </button>
          </form>
        </Modal.Body>
      </Modal>
    </div>
  );
};

export default UpdateActivity;
