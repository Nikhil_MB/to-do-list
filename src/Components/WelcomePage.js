import React, { useState } from "react";
import MyActivities from "./MyActivities";
import NewActivity from "./NewActivity";

export const WelcomePage = () => {
  const [toggleActivities, setToggleActivities] = useState(false);
  const [toggleNewActivity, setToggleNewActivity] = useState(false);

  const handleCreateActivity = () => {
    if (toggleNewActivity) {
      setToggleNewActivity(false);
    } else {
      setToggleActivities(false);
      setToggleNewActivity(true);
    }
  };

  const handleActivityList = () => {
    if (toggleActivities) {
      setToggleActivities(false);
    } else {
      setToggleNewActivity(false);
      setToggleActivities(true);
    }
  };
  const toggleAfterUpdate = () => {
    setToggleActivities(false);
    setToggleNewActivity(false);
  };
  return (
    <div>
      <button className='btn btn-success' onClick={handleCreateActivity}>
        Create new activity
      </button>
      <button
        className='btn btn-primary'
        style={{ marginLeft: "10px" }}
        onClick={handleActivityList}
      >
        My activites
      </button>
      <div>
        {toggleActivities && <MyActivities update={toggleAfterUpdate} />}
      </div>
      <div>
        {toggleNewActivity && (
          <NewActivity
            isModelOpen={toggleNewActivity}
            updateActivity={toggleAfterUpdate}
          />
        )}
      </div>
    </div>
  );
};
