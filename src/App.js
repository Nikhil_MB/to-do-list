import React from "react";
import { WelcomePage } from "./Components/WelcomePage";

function App() {
  return (
    <div className='App' style={{ textAlign: "center" }}>
      <h1>To Do List</h1>
      <WelcomePage />
    </div>
  );
}

export default App;
