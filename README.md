# Getting Started with To-Do List

In this project you can create your to-do list and keep track of your activities

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Components

It is used to display data.

There are 4 components available :-

- Welcome Page :- It is the first view in the website. It contains 2 buttons which when clicked will connect to the related components

- My Activities :- It is a view in the website that contains a list of activities. It displays that list.

- New Activities :- It is a view in the website that is used to create or add new activities to the list.

- Update Activities :- It is a view in the website that is used to update the activities of the list.

## Data

It is a data component use to store data.

It consist of 1 file :-

- activities data :- Store list of activities.

## Branches

- master
- useprops :- It is a fully optimized version of master branch using parent child properties.
- contextApi :- It is similar to useProps branch but it uses context api instead of props drilling.
